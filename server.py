import os
import requests
from bs4 import BeautifulSoup, SoupStrainer
from flask import Flask, render_template, Markup, request

app = Flask(__name__)

@app.route('/')
def hello():
    return render_template("index.html");
    
@app.route('/proxy', methods=["GET"])
def proxy():
    # get page url, fix it up
    url = request.args.get("url")
    if(not url.startswith("http")):
        url = "http://" + url
    
    # set useragent
    useragent = request.headers.get('User-Agent');
    headers = { 'User-Agent': useragent };
    
    # request page
    r = requests.get(url, headers=headers);
    
    # fix up page links
    fixedPage = fixPage(r.text, url);
    
    page = Markup(r.text);
    canvasUrl = "/canvas?url=" + url;
    # return render_template("proxy.html", pagesrc=canvasUrl);
    return render_template("canvas.html", page=Markup(fixedPage))
    
def getBaseUrl(path):
    splitUrl = path.split("/")
    newBase = ""
    for i in range(0, 3):
        newBase = newBase + splitUrl[i] + "/";
        print(newBase)
    return newBase
    
def getPathUrl(path):
    splitUrl = path.split("/")
    newPath = ""
    for i in range(0, len(splitUrl)):
        newPath = newPath + splitUrl[i] + "/";
        print(newPath)
    return newPath
    
def fixPage(page, url):
    fixedPage = page
    baseUrl = getBaseUrl(url)
    pathUrl = getPathUrl(url)
    
    print("calling fixer...");
    soup = BeautifulSoup(page, 'html.parser')
    for link in soup.find_all('a'):
        fixedPage = page.replace(link.get("href"), fixLink(link.get("href"), baseUrl, pathUrl))
    
    print("returning fixed...")
    print(fixedPage)
    return fixedPage
        
def fixLink(url, baseUrl, pathUrl):
    newUrl = url
    if(url.startswith("http://")) or (url.startswith("https://")) or (url.startswith("://")):
        newUrl = "/proxy?url=" + url # its fine
    elif(url.startswith("./")) or (url.startswith("/")):
        newUrl = "/proxy?url=" + baseUrl + url # add the base
    else:
        newUrl = "/proxy?url=" + pathUrl + url # add the path
        print("old:", url, "new:", newUrl)
    return newUrl

app.run(host=os.getenv('IP', '0.0.0.0'),port=int(os.getenv('PORT', 8080)))